QT += core
QT -= gui

CONFIG += c++11

TARGET = Braille
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp
LIBS += `pkg-config opencv --libs`
