#include <opencv2/opencv.hpp>
#include <bits/stdc++.h>

using namespace cv;
using namespace std;
typedef vector<Point> vp;
bool comp(pair<float,vp> a, pair<float,vp> b);
void le_csv(const string&arquivo, vector<Mat>&imagens,vector<string>&nomes,char separador = ';');
int main()
{
    Mat normal,image,deriv_image,layout_x,layout_y,deriv_layout,result,cpy;
    string csv = "at.txt";
    vector<Mat> imagens;
    vector<string>nomes;
    double minVal,maxVal;
    Point minLoc; Point maxLoc;
    normal = imread("9.jpg");
    cvtColor(normal,image,CV_BGR2GRAY);
    normal.copyTo(cpy);
    namedWindow("Imagem binaria",CV_WINDOW_FREERATIO);
    namedWindow("Derivada",CV_WINDOW_FREERATIO);
    namedWindow("Saida",CV_WINDOW_FREERATIO);
    if(!image.data)
    {
        cout << "Erro ao abrir a imagem" <<endl;
        return 0;
    }
    GaussianBlur(image, image, Size(5,5), 0, 0); // Filtra a imagem.
    Canny(image,deriv_image,50,200);
    imshow("Imagem binaria",deriv_image);
    waitKey(0);
    le_csv(csv,imagens,nomes);
    for(int i = 0;i < imagens.size();++i)
    {
        Canny(imagens[i],deriv_layout,50,200);
        cout << nomes[i] <<endl;
        imshow("Derivada",deriv_layout);
        waitKey(0);
        matchTemplate(deriv_image,deriv_layout,result,CV_TM_SQDIFF);
        minMaxLoc( result, &minVal, &maxVal, &minLoc, &maxLoc, Mat() );
        rectangle(cpy,minLoc,Point( minLoc.x + imagens[i].cols , minLoc.y + imagens[i].rows ),Scalar(0,0,255),5);
        putText(cpy,nomes[i],minLoc,FONT_HERSHEY_SIMPLEX,2,Scalar(0,0,255),5);
        imshow("Saida",cpy);

    }
    putText(cpy,"Weslley Rocks",Point(10,10),FONT_HERSHEY_SIMPLEX,1,Scalar(255,0,0),2);
    imwrite("Saida.png",cpy);
    waitKey(0);

    return 0;
}

bool comp(pair<float,vp> a, pair<float,vp> b){
    return a.first > b.first;

}
void le_csv(const string&arquivo, vector<Mat>&imagens,vector<string>&nomes,char separador  )
{
    ifstream arq(arquivo.c_str(),ifstream::in);
    if(!arq)
    {
        string erro = "Erro! Nome de arquivo inválido";
        return;
    }
    string linha,caminho,label,nome;
    while(getline(arq,linha))
    {
        stringstream ss(linha);
        getline(ss,caminho,separador);
        getline(ss,label,separador);
        getline(ss,nome);
        if(!caminho.empty() && !label.empty() && !nome.empty())
        {
            imagens.push_back(imread(caminho));
            nomes.push_back(nome.substr(0,nome.find("bin")));
        }
    }
}
